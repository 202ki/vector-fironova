#include <my/vector.h>
#include <iostream>
#include <gtest/gtest.h>
#include "test.h"
#include <vector>


TEST(vector, size)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    EXPECT_EQ(a.size(), 0u);
}

TEST(vector, size1)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 1000; i++)
    {
        b.push_back(i);
    }

    ASSERT_NE(a.size(), 5u);
}

TEST(vector, size2)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    EXPECT_EQ(b.size(), 6u);
}

TEST(vector, size3)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    ASSERT_NE(b.size(), 10u);
}

TEST(vector, empty)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    EXPECT_EQ(a.empty(), true);

}

TEST(vector, empty1)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 1000; i++)
    {
        b.push_back(i);
    }

    ASSERT_NE(a.empty(), false);
}

TEST(vector, empty2)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    ASSERT_NE(b.empty(), true);
}

TEST(vector, empty3)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    EXPECT_EQ(b.empty(), false);
}


TEST(vector, capacity)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    ASSERT_NE(a.capacity(), 1u);
}

TEST(vector, capacity1)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    EXPECT_EQ(a.capacity(), 0u);
}

TEST(vector, capacity2)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    ASSERT_NE(b.capacity(), 0u);
}

TEST(vector, capacity3)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    ASSERT_NE(a.capacity(), 6u);
}


TEST(vector, shrink_to_fit)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }
    
    a.shrink_to_fit();

    EXPECT_EQ(a.capacity(), a.size());
}

TEST(vector, shrink_to_fit1)
{
    vector<int> a;

    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }

    b.shrink_to_fit();

    EXPECT_EQ(b.capacity(), b.size());
}


TEST(vector, erase)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b;

    for (int i = 1; i < 7; i++)
    {
        b.push_back(i);
    }
    
    std::vector<int> c (b.begin(), b.end());

    b.erase(b.begin() + 2, b.begin() + 3);


    a.erase(a.begin() + 2, a.begin() + 3);

//   EXPECT_EQ(a.erase(a.begin() + 2, a.begin() + 4) - a.begin(),  b.erase(b.begin() + 2, b.begin() + 4) - b.begin());

}

TEST(vector, erase1)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b;

    for (int i = 1; i < 7; i++)
    {
        b.push_back(i);
    }


    b.erase(b.begin() + 2, b.begin() + 3);

    std::vector<int> c (b.begin(), b.end());

    a.erase(a.begin() + 2, a.begin() + 3);

    EXPECT_EQ(a.erase(a.begin() + 2, a.begin() + 4) - a.begin(),  b.erase(b.begin() + 2, b.begin() + 4) - b.begin());

}

TEST(vector, erase2)
{

    std::vector<int> a = {0};
    vector<int> b;

    for (int i = 1; i < 7; i++)
    {
        b.push_back(i);
    }


    b.erase(b.begin() + 2, b.begin() + 3);

    std::vector<int> c (b.begin(), b.end());

//    a.erase(a.begin() + 2, a.begin() + 3);

    ASSERT_NE(a.end() - a.begin(),  b.erase(b.begin() + 2, b.begin() + 4) - b.begin());

}

TEST(vector, erase3)
{

    std::vector<int> a = {1, 2, 4, 5, 6};
    vector<int> b;

    for (int i = 1; i < 7; i++)
    {
        b.push_back(i);
    }

    b.erase(b.begin() + 5, b.begin() + 6);

//    std::vector<int> c (b.begin(), b.end());

//    a.erase(a.begin() + 2, a.begin() + 3);

    EXPECT_EQ(a.end() - a.begin(), b.end() - b.begin());

}

TEST(vector, erase111)
{

    std::vector<int> a = {1, 2, 4, 5, 6};
    vector<vector<int>> b;
    vector<int> c;

    for (int i = 1; i < 7; i++)
    {
        c.push_back(i);
    }

    for (int i = 1; i < 7; i++)
    {
        b.push_back(c);
    }

    b.erase(b.begin() + 5, b.begin() + 6);

//    std::vector<int> c (b.begin(), b.end());

//    a.erase(a.begin() + 2, a.begin() + 3);

    EXPECT_EQ(a.end() - a.begin(), b.end() - b.begin());

}

TEST(vector, erase112)
{

    std::vector<int> a = {1, 2, 4, 5, 6};
    vector<vector<std::string>> b;
    vector<std::string> c;

    for (int i = 1; i < 7; i++)
    {
        c.push_back("a");
    }

    for (int i = 1; i < 7; i++)
    {
        b.push_back(c);
    }

    b.erase(b.begin() + 5, b.begin() + 6);

//    std::vector<int> c (b.begin(), b.end());

//    a.erase(a.begin() + 2, a.begin() + 3);

    EXPECT_EQ(a.end() - a.begin(), b.end() - b.begin());
}

TEST(vector, erase4)
{

    std::vector<int> a = {};
    vector<int> b;

    for (int i = 1; i < 7; i++)
    {
        b.push_back(i);
    }


    b.erase(b.begin(), b.begin() + 6);

//    std::vector<int> c (b.begin(), b.end());


    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());

}

TEST(vector, erase5)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b;

    for (int i = 1; i < 7; i++)
    {
        b.push_back(i);
    }


    a.erase(a.begin(), a.begin() + 6);
    b.erase(b.begin(), b.begin() + 6);

//    std::vector<int> c (b.begin(), b.end());


    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());

}

TEST(vector, erase6)
{

    std::vector<int> a = {0};
    vector<int> b;

    for (int i = 1; i < 7; i++)
    {
        b.push_back(i);
    }


    a.erase(a.begin(), a.begin() + 1);
    b.erase(b.begin(), b.begin() + 6);

//    std::vector<int> c (b.begin(), b.end());


    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());

} 

TEST(vector, erase7)
{

    std::vector<int> a = {5};
    vector<int> b;

    for (int i = 0; i < 1; i++)
    {
        b.push_back(i);
    }


    a.erase(a.begin(), a.begin() + 1);
    b.erase(b.begin(), b.begin() + 1);

//    std::vector<int> c (b.begin(), b.end());


    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());

}

TEST(vector, erase8)
{

    std::vector<int> a = {};
    vector<int> b;

    // for (int i = 1; i < 7; i++)
    // {
    //     b.push_back(i);
    // }


 //   b.erase(b.begin() + 2, b.begin() + 3);

  //  std::vector<int> c (b.begin(), b.end());

 //   a.erase(a.begin() + 2, a.begin() + 3);

//    EXPECT_EQ(a, c);
    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());

}

TEST(vector, erase9)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b;

    // for (int i = 1; i < 7; i++)
    // {
    //     b.push_back(i);
    // }

    a.erase(a.begin(), a.begin() + 6);

  //  std::vector<int> c (b.begin(), b.end());

 //   a.erase(a.begin() + 2, a.begin() + 3);

//    EXPECT_EQ(a, c);
    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());
}

TEST(vector, erase10)
{

    std::vector<std::string> a = {"a"};
    vector<int> b;
    for (int i = 0; i < 1; i++)
    {
        b.push_back(i);
    }


    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());
}

TEST(vector, erase11)
{

    std::vector<int> a = {1, 2, 3, 4, 5};
    vector<std::string> b;

    for (int i = 1; i < 7; i++)
    {
        b.push_back("a");
    }


    b.erase(b.begin() + 2, b.begin() + 3);

  //  std::vector<int> c (b.begin(), b.end());

 //   a.erase(a.begin() + 2, a.begin() + 3);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());

}


TEST(vector, assign)
{

    std::vector<int> a = {1, 2, 3};
    vector<int> b;

    a.assign(0, 3);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      
}

TEST(vector, assign1)
{

    std::vector<int> a = {1, 1, 1, 1, 1};
    vector<int> b;

    b.assign(5, 1);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      
}

TEST(vector, assign2)
{

    std::vector<int> a = {1, 2, 1, 1, 1};
    vector<int> b;

    b.assign(5, 1);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      
}
/*
TEST(vector, assign3)
{

    std::vector<std::string> a = {"a", "a", "a"};
    vector<std::string> b;

    b.assign(3, "a");

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      
}
*/
TEST(vector, assign4)
{

    std::vector<int> a = {1, 2, 1, 1, 1};
    vector<int> b;

    b.assign(6, 1);

    ASSERT_NE(a.end() - a.begin(),  b.end() - b.begin());      

}

// TEST(vector, assign5)
// {

//     std::vector<std::string> a = {"a"};
//     vector<std::string> b;

//     b.assign(1, "a");

//     EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

// }

TEST(vector, assign6)
{

    std::vector<int> a = {1};
    vector<std::string> b;

    for (int i = 0; i < 5; i++)
    {
        b.push_back("a");
    }

    b.assign(1, "a");

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, assign7)
{

    std::vector<int> a = {1, 2, 3, 4, 5};
    vector<std::string> b;

    for (int i = 0; i < 5; i++)
    {
        b.push_back("a");
    }

    b.assign(5, "a");

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}
/*
TEST(vector, assign8)
{

    vector<int> a = {1, 2, 3};
    vector<int> b;

    b.assign(a.begin(), a.begin() + 3);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}
*/
/*  
TEST(vector, assign9)
{
    std::vector<int> a = {1, 2, 3, 4, 5};
    vector<vector<int>> b;
    vector<int> c = {1, 1, 1};
    
    
    c.assign(a.begin(), a.begin() + 4);
    for (int i = 1; i < 7; i++)
    {
        b.push_back(c);
    }

   

    EXPECT_EQ(b.size(), 5u);

}
*/
TEST(vector, clear)
{

    std::vector<int> a = {1, 2, 3, 4, 5};
    vector<int> b;

    for (int i = 0; i < 5; i++)
    {
        b.push_back(i);
    }

    a.assign(0, 0);
    b.clear();

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, clear1)
{

    std::vector<int> a = {};
    vector<std::string> b;

    for (int i = 0; i < 5; i++)
    {
        b.push_back("aa");
    }
    
    b.clear();

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, clear2)
{

    std::vector<int> a = {};
    vector<std::string> b;
   
    b.clear();

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, clear3)
{

    std::vector<int> a = {};
    vector<vector<int>> b;
    vector<int> c;
    
    for (int i = 0; i < 7; i++)
    {
        b.push_back(c);
    }

    b.clear();

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, insert)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b;

    for ( int i = 1; i < 6; i++)
    {
        b.push_back(i);
    }
   
    b.insert(b.begin(), 1);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}


TEST(vector, insert1)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b;

    for ( int i = 1; i < 6; i++)
    {
        b.push_back(i);
    }
   
    b.insert(b.end(), 1);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, insert2)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b;

    for ( int i = 1; i < 6; i++)
    {
        b.push_back(i);
    }
   
    b.insert(b.begin() + 1, 1);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, insert3)
{

    std::vector<int> a = {1};
    vector<int> b;
   
    b.insert(b.end(), 1);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, insert4)
{

    std::vector<int> a = {1, 2};
    vector<int> b;
   
    b.insert(b.begin(), 1);

    ASSERT_NE(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, insert5)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b;
    for (int i = 1; i < 6; i++)
    {
        b.push_back(i);
    }
   
    b.insert(b.begin() + 1, 1);

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, insert6)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<std::string> b;
    for (int i = 1; i < 6; i++)
    {
        b.push_back("a");
    }
   
    b.insert(b.begin() + 1, "1");

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, insert7)
{

    std::vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<std::string> b;
    for (int i = 1; i < 6; i++)
    {
        b.push_back("i");
    }
   
    b.insert(b.begin() + 1, 1,"3");

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, insert8)
{

    std::vector<int> a = {1, 2, 3};
    vector<std::string> b;
    // for (int i = 1; i < 6; i++)
    // {
    //     b.push_back("i");
    // }
   
    b.insert(b.begin(), 3, "3");

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

/*
TEST(vector, insert9)
{

    std::vector<int> a = {};
    vector<std::string> b;
    // for (int i = 1; i < 6; i++)
    // {
    //     b.push_back("i");
    // }
   
    b.insert(b.end() - 1, 0,"3");

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

*/

TEST(vector, insert10)
{

    vector<int> a = {1, 2, 3, 4, 5, 6};
    vector<int> b = {0, 0, 0, 0};

    b.insert(b.begin() + 2, a.begin(), a.begin() + 4);

    EXPECT_EQ(a.size(), b.size());      

}



TEST(vector, pop_back)
{
    std::vector<int> a = {1, 2, 3, 4, 5};
    vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }
   
    b.pop_back();

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, pop_back1)
{
    std::vector<int> a = {};
    vector<int> b;
    for (int i = 0; i < 1; i++)
    {
        b.push_back(i);
    }
   
    b.pop_back();

    EXPECT_EQ(a.end() - a.begin(),  b.end() - b.begin());      

}

TEST(vector, resize)
{
    std::vector<int> a = {1, 2, 3};
    vector<int> b;
    for (int i = 0; i < 5; i++)
    {
        b.push_back(i);
    }
    
    b.resize(3);

    EXPECT_EQ(b.end() - b.begin(), a.end() - a.begin());
}

TEST(vector, resize1)
{
    std::vector<int> a = {1, 2, 3};
    vector<std::string> b;
    for (int i = 0; i < 5; i++)
    {
        b.push_back("i");
    }
    
    b.resize(3);

    EXPECT_EQ(b.end() - b.begin(), a.end() - a.begin());
}

TEST(vector, resize2)
{
    std::vector<int> a = {1, 2, 3};
    vector<std::string> b;
    // for (int i = 0; i < 5; i++)
    // {
    //     b.push_back("i");
    // }
    
    b.resize(3, "2");

    EXPECT_EQ(b.end() - b.begin(), a.end() - a.begin());
}

TEST(vector, resize3)
{
    std::vector<int> a = {1, 2, 3};
    vector<vector<int>> b;
    vector<int> c;
    for (int i = 0; i < 5; i++)
    {
        b.push_back(c);
    }
    
    b.resize(3);

    EXPECT_EQ(b.end() - b.begin(), a.end() - a.begin());
}

/*
TEST(vector, operator_ravno)
{
    vector<int> a = {1, 2, 3};
    vector<int> c = {1, 2, 3};
    
    EXPECT_EQ((a == c), true);
}
*/

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}