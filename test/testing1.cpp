#include "vector1.h"
#include <iostream>
#include "gtest/gtest.h"
#include "test.h"
#include <vector>
#include <typeinfo>
 
 int main()
 {
 
 
    my::Test t1;
    t1.number = 1;

    my::Test t2;
    t2.number = 2;

    my::Test t3;
    t3.number = 3;

    my::Test t4;
    t4.number = 4;

    my::Test t5;
    t5.number = 5;

    my::Test t6;
    t6.number = 6;

    my::Test t7;
    t7.number = 7;

    my::Test t8;
    t8.number = 8;

    my::Test t9;
    t9.number = 9;

    my::Test t10;
    t10.number = 10;

    my::Test t11;
    t11.number = 11;

    my::Test t12;
    t12.number = 12;

    my::Test t13;
    t13.number = 13;

    my::Test t14;
    t14.number = 14;

    std::vector<my::Test> a;
    std::vector<my::Test> c;

    vector<my::Test> b;
    vector<my::Test> d;

    a.push_back(t1);
    a.push_back(t2);
    a.push_back(t3);
    a.push_back(t4);
    a.push_back(t5);
    a.push_back(t6);

    b.push_back(t1);
    b.push_back(t2);
    b.push_back(t3);
    b.push_back(t4);
    b.push_back(t5);
    b.push_back(t6);



    c.push_back(t7);
    c.push_back(t8);
    c.push_back(t9);
    c.push_back(t10);
    c.push_back(t11);
    c.push_back(t12);
    c.push_back(t13);
    c.push_back(t14);


    d.push_back(t7);
    d.push_back(t8);
    d.push_back(t9);
    d.push_back(t10);
    d.push_back(t11);
    d.push_back(t12);
    d.push_back(t13);
    d.push_back(t14);



/*
    a.clear();
    b.clear();

    std::cout << "C++ vector capacity: " << a.capacity() << std::endl;
    std::cout << "C++ vector size: "     << a.size()     << std::endl;

    std::cout << "My vector capacity: " << b.capacity() << std::endl;
    std::cout << "My vector size: "     << b.size()     << std::endl;
*/

/*
    std::cout << "-------------------------" << std::endl;
    a.insert(a.begin() + 3, 8, 10);
    std::cout << "C++ vector capacity: " << a.capacity() << std::endl;
    std::cout << "C++ vector size: "     << a.size()     << std::endl;

    b.insert(b.begin() + 3, 8, 10);
    std::cout << "My vector capacity: " << b.capacity() << std::endl;
    std::cout << "My vector size: "     << b.size()     << std::endl;
*/

/*
    std::cout << "-------------------------" << std::endl;
    a.push_back(7);
    std::cout << "C++ vector capacity: " << a.capacity() << std::endl;
    std::cout << "C++ vector size: "     << a.size()     << std::endl;

    std::cout << "-------------------------" << std::endl;
    b.push_back(7);
    std::cout << "My vector capacity: " << b.capacity() << std::endl;
    std::cout << "My vector size: "     << b.size()     << std::endl;
*/

/*std::cout << "-------------------------" << std::endl;
    a.assign(c.begin(), c.end());
    std::cout << "C++ vector capacity: " << a.capacity() << std::endl;
    std::cout << "C++ vector size: "     << a.size()     << std::endl;

    std::cout << "-------------------------" << std::endl;
    
    std::cout << "hhhh"<<typeid(d.begin()).name() <<" "<< typeid(d.end() - 1).name() << '\n';
    

    b.assign(d.begin(), d.end());
    std::cout << "My vector capacity: " << b.capacity() << std::endl;
    std::cout << "My vector size: "     << b.size()     << std::endl;
*/
    
 

/*

    std::cout << "-------------------------" << std::endl;
    a.insert(a.begin() + 2, 10, 0);
    std::cout << "C++ vector capacity: " << a.capacity() << std::endl;
    std::cout << "C++ vector size: "     << a.size()     << std::endl;

    std::cout << "-------------------------" << std::endl;
    b.insert(b.begin() + 2, 10, 0);
    std::cout << "My vector capacity: " << b.capacity() << std::endl;
    std::cout << "My vector size: "     << b.size()     << std::endl;

    std::cout << "-------------------" << std::endl;

    std::cout << "Вывод собственного вектора" << std::endl;
    for(int i = 0; i < b.size(); i++)
    std::cout << b[i].number << std::endl;

    std::cout << "Вывод вектора c++" << std::endl;
    for(int i = 0; i < a.size(); i++)
    std::cout<< a[i].number << std::endl;

*/
/*
a.emplace(a.begin() + 2, 44);

std::cout <<  "------------------" << std::endl;

b.emplace(b.begin() + 2, 44);
*/


/*
std::cout << "-------------------------" << std::endl;
    a.resize(20);

    std::cout << "C++ vector capacity: " << a.capacity() << std::endl;
    std::cout << "C++ vector size: "     << a.size()     << std::endl;


    std::cout << "-------------------------" << std::endl;
    b.resize(20);
    std::cout << "My vector capacity: " << b.capacity() << std::endl;
    std::cout << "My vector size: "     << b.size()     << std::endl;
*/
/*
    vector<int> d;
    vector<int> e;

    d.push_back(1);
    d.push_back(2);
    d.push_back(3);
    d.push_back(4);
    d.push_back(5);


    e.push_back(1);
    e.push_back(2);
    e.push_back(3);
    e.push_back(5);
    e.push_back(5);
    e.push_back(5);

    d.assign(5, 0);


    std::cout << d.size() << " " << d.capacity() << std::endl;
*/

/*

    std::cout << "Выводим результат вектора с++"<< std::endl << (*(a.erase(a.begin() + 3))).number << std::endl << "Выводим результат собственного вектора" << std::endl << (*(b.erase(b.begin() + 3))).number << std::endl;
*/
/*
    std::cout << "Выводим результат вектора с++"<< std::endl << *(a.clear()) << std::endl << "Выводим результат собственного вектора" << std::endl <<*(b.clear()) << std::endl;
    
    std::cout << "Конечные деструкторы при выходе из программы" << std::endl;
/*
/*
    std::cout << "Вывод собственного вектора" << std::endl;
    for(int i = 0; i < b.size(); i++)
    std::cout << b[i].number << std::endl;

    std::cout << "Вывод вектора c++" << std::endl;
    for(int i = 0; i < a.size(); i++)
    std::cout<< a[i].number << std::endl;


    std::cout << a.size() << std::endl;
    std::cout << b.size() << std::endl;
*/


/*
vector<int> a;
    for (int i = 0; i < 6; i++)
    {
        a.push_back(i);
    }

    std::vector<int> b;
    for (int i = 0; i < 6; i++)
    {
        b.push_back(i);
    }
    std::cout << *(a.erase(a.begin() + 1)) << " " << *(b.erase(b.begin() + 1)) << std::endl;
*/

/*
    std::vector<std::string> ff = {"a", "a", "a"};
    vector<std::string> l;

    l.assign(3, "a");

    std::cout << "-------------------" << std::endl;
    std::cout << ff.end() - ff.begin() << std::endl;
    std::cout << "-------------------" << std::endl;
    std::cout << l.end() - l.begin() << std::endl;
    std::cout << "-------------------" << std::endl;
    std::cout << "Вывод собственного вектора" << std::endl;
    for(int i = 0; i < l.size(); i++)
    std::cout << l[i] << std::endl;

*/
    vector<int> t = {1, 2, 3, 4};
    vector<int> m = {1, 2, 3, 4};

    vector<vector<int>> v;
    v.push_back(t);
    v.push_back(m);


/*
 vector<std::string> l;

    for (int i = 0; i < 5; i++)
    {
        l.push_back("a");
    }
    std::cout << "-------------------" << std::endl;
    std::cout << l.size() << " " <<l.capacity() << std::endl;

    l.assign(6, "a");
    std::cout << "-------------------" << std::endl;
    std::cout << l.size() << std::endl;
    std::cout << "-------------------" << std::endl;
    std::cout << "Вывод собственного вектора" << std::endl;
    for(int i = 0; i < l.size(); i++)
    std::cout << l[i] << std::endl;
*/





/*
    vector<std::string> h;   
    h.insert(h.begin(), 3, "3");
    std::cout << h.size() << std::endl;
    std::cout << "-------------------" << std::endl;
    std::cout << "Вывод собственного вектора" << std::endl;
    for(int i = 0; i < h.size(); i++)
    std::cout << h[i] << std::endl;

*/







 }